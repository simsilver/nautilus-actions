# NautilusActions

## (Nautilus extension)

A simple nautilus extension adding menu to the right-click for the selected files,
like filemanager-actions.

config json file should be put into ~/.config/NautilusActions, with name like
'action-name.json' and contents like below

    {
     "name": "Action Name",
     "cmd": "some command on file %f",
     "mime": ["*/*"],
     "show_result": False,
     "work_dir": "path start with '/' or '~' or relative path",
     "selection": "expression like '%c > 1' about count of selected files"
    }

Dependency: `python-nautilus`

