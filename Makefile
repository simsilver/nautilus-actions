nautilus_path=`which nautilus`
src := nautilus-actions.py
dst_dir := ~/.local/share/nautilus-python/extensions/

install: $(src)
	mkdir -p $(dst_dir)
	cp -r $? $(dst_dir)
	@echo 'Restarting nautilus'
	@${nautilus_path} -q||true # This is due to nautilus -q always returning 255 status which causes makefile to think it failed

uninstall:
	pushd $(dst_dir) && rm -rf $(src) && popd
	@echo 'Restarting nautilus'
	@${nautilus_path} -q||true # This is due to nautilus -q always returning 255 status which causes makefile to think it failed
