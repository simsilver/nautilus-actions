# -*- coding: utf-8 -*-
#
import fnmatch
import glob
import json
import logging
import logging.handlers
import os
import shlex
import subprocess
import sys
from urllib import parse

import gi

try:
    gi.require_version("Gtk", "4.0")
    gi.require_version("Nautilus", "4.0")
except ValueError:
    gi.require_version("Gtk", "3.0")
    gi.require_version("Nautilus", "3.0")
finally:
    gi.require_version("Notify", "0.7")
    from gi.repository import Nautilus, GObject, Notify

debug = False


def prepare_logger(name: str) -> logging.Logger:
    handler = logging.handlers.WatchedFileHandler("/tmp/NautilusActions.log")
    formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s:%(message)s')
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.addHandler(handler)

    if debug:
        logger.setLevel("DEBUG")
    else:
        logger.setLevel(os.environ.get("LOGLEVEL", "WARN"))

    return logger


class NautilusActionsExtension(GObject.GObject, Nautilus.MenuProvider):
    """Add Actions for selecting file"""

    def __init__(self):
        self.actions = {}
        self.logger = prepare_logger("NAction")
        self.logger.info("NautilusActionsExtension init")
        self._load_actions()

    def _load_actions(self):
        config_dir = os.path.join(os.path.expanduser('~'), '.config/NautilusActions')
        os.makedirs(config_dir, exist_ok=True)
        for file in glob.glob('*.json', root_dir=config_dir):
            full_name = os.path.join(config_dir, file)
            action = self._load_one_action(full_name)
            self.logger.debug('loading from json file %s got %s', full_name, action)
            if action:
                self.actions[file] = action

        if len(self.actions) == 0:
            action_example_file = os.path.join(config_dir, 'action_name.json.example')
            if not os.path.exists(action_example_file):
                with open(action_example_file, 'w') as w:
                    self.logger.debug('write action example file')
                    example = {'name': 'Action Name',
                               'cmd': 'some command on file %f',
                               'mime': ['*/*'],
                               'show_result': False,
                               'work_dir': "path start with '/' or '~' or relative path",
                               'selection': "expression like '%c > 1' about count of selected files"
                               }
                    json.dump(example, w, indent=4)

    @staticmethod
    def _load_one_action(full_name: str) -> dict | None:
        with open(full_name, mode='r') as f:
            action = json.load(f)
            if action.get('name') and action.get('cmd') and action.get('mime'):
                return action
        return None

    def get_file_items(self, *args):
        """Nautilus invoke this function when building the menu on files."""
        self.logger.info("NautilusActionsExtension get_file_items")
        file_items = args[-1]
        if not len(file_items):
            return
        return self._generate_menu(file_items)

    def get_background_items(self, *args):
        """Nautilus invoke this function when building the menu on the empty
        background."""
        self.logger.info("NautilusActionsExtension get_background_items")
        pass

    def _generate_menu(self, file_items):
        self.files = []
        all_item_action_set = set(self.actions.keys())
        for item in file_items:
            cur_item_action_list = self._add_item_actions(item)
            if cur_item_action_list:
                all_item_action_set = all_item_action_set.intersection(set(cur_item_action_list))
            else:
                all_item_action_set = set()

        menu_item_list = []
        for action_key in all_item_action_set:
            pass_count_check = self._check_count(action_key, len(file_items))
            if pass_count_check:
                cur_menu_item = self._add_item_menu(action_key)
                menu_item_list.append(cur_menu_item)

        if menu_item_list:
            self.files = file_items

        return menu_item_list

    def _add_item_actions(self, item) -> list[str]:
        item_mime = item.get_mime_type()
        item_uri = item.get_uri()
        keys = []
        self.logger.debug("check actions for uri %s", item_uri)
        for key in self.actions.keys():
            test_count = 0
            action_info = self.actions[key]
            mime_list = action_info['mime']
            self.logger.debug("action %s with conf %s", key, action_info)

            if mime_list:
                self.logger.debug("item_mime %s, mime_list %s", item_mime, mime_list)
                test_count += 1
                found = False
                for mime_pattern in mime_list:
                    if fnmatch.fnmatch(item_mime, mime_pattern):
                        self.logger.debug("match %s", mime_pattern)
                        found = True
                        break
                if not found:
                    self.logger.debug("no support mime found")
                    continue

            if test_count > 0:
                keys.append(key)

        return keys

    def _check_count(self, action_key: str, count: int) -> bool:
        action_info = self.actions[action_key]
        count_condition = action_info.get('selection')
        pass_count_check = not count_condition
        if count_condition:
            exp = count_condition.replace('%c', str(count))
            try:
                result = eval(exp)
                if isinstance(result, bool):
                    pass_count_check = result
                else:
                    self.logger.error('Expression got %s when eval %s from %s', result, exp, count_condition)
            except SyntaxError as e:
                self.logger.error('Error %s when eval %s from %s', e, exp, count_condition)

        return pass_count_check

    def _add_item_menu(self, action_key: str):
        item_name = 'ActionsMenu' + action_key
        item_label = self.actions[action_key]['name']
        menu_item = Nautilus.MenuItem(name=item_name, label=item_label, sensitive=True)
        menu_item.connect('activate', self._open_with_action, action_key)
        return menu_item

    def _open_with_action(self, menuitem, action_key: str):
        action_info = self.actions[action_key]
        cmd = action_info['cmd']
        action_name = action_info['name']
        work_dir: str | None = action_info.get('work_dir')
        file_paths = []
        for file in self.files:
            uri = parse.urlparse(file.get_uri())
            final_path = os.path.abspath(os.path.join(uri.netloc, parse.unquote(uri.path)))
            file_paths.append(final_path)

        first_file_dir = os.path.dirname(file_paths[0])
        if work_dir:
            if not work_dir.startswith('/') and not work_dir.startswith('~'):
                work_dir = os.path.join(first_file_dir, work_dir)
            work_dir = os.path.abspath(work_dir)
            os.makedirs(work_dir, exist_ok=True)

        if not work_dir or not os.path.exists(work_dir) or not os.path.isdir(work_dir):
            work_dir = first_file_dir

        show_result = str(action_info.get('show_result'))

        full_path = ['python', __file__, action_name, cmd, show_result]
        full_path.extend(file_paths)
        self.logger.debug('starting python in dir %s', work_dir)
        subprocess.Popen(full_path, cwd=work_dir)


class NautilusActionRunner:

    def __init__(self):
        self.logger = prepare_logger("NARunner")

    def _run_command(self, cmd: str, files: list[str], action_name: str, show_result: bool):
        self.logger.debug('enter _run_command')
        cmd_full = cmd.replace('%f', ' '.join(shlex.quote(file) for file in files))
        self.logger.info('starting cmd "%s"', cmd_full)
        pipe = subprocess.Popen(cmd_full, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        self.logger.debug('_run_command try communicate')
        stdout, stderr = pipe.communicate()

        str_out = ''
        str_err = ''

        self.logger.debug('_run_command action "%s" cmd "%s" got %d', action_name, cmd_full, pipe.returncode)
        if stdout:
            str_out = stdout.decode()
        if stderr:
            str_err = stderr.decode()
        self.logger.debug('_run_command stdout background %s', str_out)
        self.logger.debug('_run_command stderr background %s', str_err)

        if show_result:
            self._send_notify(action_name, str_out + '\n' + str_err)

    def run(self, args: list[str]):
        action_name: str = args[0]
        cmd: str = args[1]
        show_result: bool = args[2] == 'True'
        files: list[str] = args[3:]
        self._run_command(cmd, files, action_name, show_result)

    @staticmethod
    def _send_notify(summary: str, body: str = ""):
        Notify.init("Nautilus-Actions")

        n = Notify.Notification.new(
            summary,
            body,
            'dialog-information'
        )
        n.show()


if __name__ == '__main__':
    runner: NautilusActionRunner = NautilusActionRunner()
    runner.run(sys.argv[1:])
